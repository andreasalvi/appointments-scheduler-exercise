# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.8-MariaDB)
# Database: appointments
# Generation Time: 2020-02-24 06:29:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Appointment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Appointment`;

CREATE TABLE `Appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `creator_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK832y0my62gdxee9md5v74v0im` (`creator_id`),
  CONSTRAINT `FK832y0my62gdxee9md5v74v0im` FOREIGN KEY (`creator_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `Appointment` WRITE;
/*!40000 ALTER TABLE `Appointment` DISABLE KEYS */;

INSERT INTO `Appointment` (`id`, `date`, `description`, `creator_id`)
VALUES
	(1,'2020-02-03 10:00:00','Production meeting',7),
	(2,'2020-02-12 15:00:00','Tech meeting',1),
	(3,'2020-02-15 17:00:00','Bring Junior around',4),
	(4,'2020-02-10 18:00:00','Call Walter',3),
	(5,'2020-03-10 20:00:00','Donation party',7),
	(6,'2020-02-13 16:00:00','Money issues',1),
	(7,'2020-03-01 08:00:00','Investigate on WW',6),
	(8,'2020-02-10 16:00:00','Teachers meething',1);

/*!40000 ALTER TABLE `Appointment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Meeting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Meeting`;

CREATE TABLE `Meeting` (
  `isRecurrent` bit(1) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK8bft0uiof95yjcegvgovqspqi` FOREIGN KEY (`id`) REFERENCES `Appointment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `Meeting` WRITE;
/*!40000 ALTER TABLE `Meeting` DISABLE KEYS */;

INSERT INTO `Meeting` (`isRecurrent`, `id`)
VALUES
	(b'1',1),
	(b'1',2),
	(b'0',5),
	(b'0',6),
	(b'0',8);

/*!40000 ALTER TABLE `Meeting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Participation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Participation`;

CREATE TABLE `Participation` (
  `meeting_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `FK5wghc6y8l2txd2qb359ksrrug` (`user_id`),
  KEY `FKqgq5uoohmo1ah3j8j2pfwlhfw` (`meeting_id`),
  CONSTRAINT `FK5wghc6y8l2txd2qb359ksrrug` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FKqgq5uoohmo1ah3j8j2pfwlhfw` FOREIGN KEY (`meeting_id`) REFERENCES `Meeting` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `Participation` WRITE;
/*!40000 ALTER TABLE `Participation` DISABLE KEYS */;

INSERT INTO `Participation` (`meeting_id`, `user_id`)
VALUES
	(1,1),
	(1,2),
	(2,2),
	(5,6),
	(6,2),
	(6,3);

/*!40000 ALTER TABLE `Participation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Reminder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Reminder`;

CREATE TABLE `Reminder` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKrrwwbxfemmea97m62gs431e5e` FOREIGN KEY (`id`) REFERENCES `Appointment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `Reminder` WRITE;
/*!40000 ALTER TABLE `Reminder` DISABLE KEYS */;

INSERT INTO `Reminder` (`id`)
VALUES
	(3),
	(4),
	(7);

/*!40000 ALTER TABLE `Reminder` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table User
# ------------------------------------------------------------

DROP TABLE IF EXISTS `User`;

CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `fullName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;

INSERT INTO `User` (`id`, `email`, `fullName`)
VALUES
	(1,'walter.white@bb.com','Walter White'),
	(2,'jasse.pinkman@bb.com','Jasse Pinkman'),
	(3,'saul.goodman@bb.com','Saul Goodman'),
	(4,'skyler.white@bb.com','Skyler White'),
	(5,'hank.schrader@bb.com','Hank Schrader'),
	(6,'mike.ehrmantraut@bb.com','Mike Ehrmantraut'),
	(7,'gustavo.fring@bb.com','Gustavo Fring');

/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

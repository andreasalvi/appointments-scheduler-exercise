INSERT INTO `User` (`id`, `email`, `fullName`)
VALUES
	(1, 'walter.white@bb.com', 'Walter White'),
	(2, 'jasse.pinkman@bb.com', 'Jasse Pinkman'),
	(3, 'saul.goodman@bb.com', 'Saul Goodman'),
	(4, 'skyler.white@bb.com', 'Skyler White'),
	(5, 'hank.schrader@bb.com', 'Hank Schrader'),
	(6, 'mike.ehrmantraut@bb.com', 'Mike Ehrmantraut'),
	(7, 'gustavo.fring@bb.com', 'Gustavo Fring');

INSERT INTO `Appointment` (`id`, `date`, `description`, `creator_id`)
VALUES
	(1, '2020-02-03 10:00:00', 'Production meeting', 7),
	(2, '2020-02-12 15:00:00', 'Tech meeting', 1),
	(3, '2020-02-15 17:00:00', 'Bring Junior around', 4),
	(4, '2020-02-10 18:00:00', 'Call Walter', 3),
	(5, '2020-03-10 20:00:00', 'Donation party', 7),
	(6, '2020-02-13 16:00:00', 'Money issues', 1),
	(7, '2020-03-01 08:00:00', 'Investigate on WW', 6),
	(8, '2020-02-10 16:00:00', 'Teachers meething', 1);

INSERT INTO `Meeting` (`id`, `isRecurrent`)
VALUES
	(1, 1),
	(2, 1),
	(5, 0),
	(6, 0);

INSERT INTO `Reminder` (`id`)
VALUES
	(3),
	(4),
	(7);

INSERT INTO `Participation` (`meeting_id`, `user_id`)
VALUES
	(1, 1),
	(1, 2),
	(2, 2),
	(5, 6),
	(6, 2),
	(6, 3);



package com.verizon.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import com.verizon.models.Appointment;
import com.verizon.models.Meeting;
import com.verizon.models.Reminder;

public class AppointmentDao extends GenericDao<Appointment> {

    public List<Appointment> getAppointmentsByDateAndUserId(Date date, Integer userId) {
        List<Appointment> results = new ArrayList<Appointment>(0);

        TypedQuery<Meeting> queryOnMeeting = this.jpaApi.em()
                .createQuery("SELECT distinct m FROM Meeting as m left join m.participants p WHERE "
                        + "((m.isRecurrent=TRUE AND dayname(m.date)=dayname(:date) AND m.date<=:date)"
                        + "OR CAST(m.date AS DATE)=CAST(:date AS DATE)) AND (m.creator.id=:userId OR p.id=:userId)",
                        Meeting.class)
                .setParameter("date", date).setParameter("userId", userId);
        results.addAll(queryOnMeeting.getResultList());

        TypedQuery<Reminder> queryOnReminder = this.jpaApi.em()
                .createQuery(
                        "SELECT r FROM Reminder as r "
                                + "WHERE CAST(r.date AS DATE)=CAST(:date AS DATE) AND r.creator.id=:userId",
                        Reminder.class)
                .setParameter("date", date).setParameter("userId", userId);
        results.addAll(queryOnReminder.getResultList());

        return results;
    }

    public List<Appointment> getAppointmentsByDate(Date date) {
        List<Appointment> results = new ArrayList<Appointment>(0);

        TypedQuery<Meeting> queryOnMeeting = this.jpaApi.em()
                .createQuery("SELECT m FROM Meeting as m WHERE "
                        + "(m.isRecurrent=TRUE AND dayname(m.date)=dayname(:date) AND m.date<=:date)"
                        + "OR CAST(m.date AS DATE)=CAST(:date AS DATE)", Meeting.class)
                .setParameter("date", date);
        results.addAll(queryOnMeeting.getResultList());

        TypedQuery<Reminder> queryOnReminder = this.jpaApi.em()
                .createQuery("SELECT r FROM Reminder as r WHERE CAST(r.date AS DATE)=CAST(:date AS DATE)",
                        Reminder.class)
                .setParameter("date", date);
        results.addAll(queryOnReminder.getResultList());

        return results;
    }

}

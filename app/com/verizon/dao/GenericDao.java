package com.verizon.dao;

import java.lang.reflect.ParameterizedType;

import javax.inject.Inject;

import com.verizon.models.GenericModel;

import play.db.jpa.JPAApi;

public class GenericDao<M extends GenericModel> {

    @Inject
    public JPAApi jpaApi;

    @SuppressWarnings("unchecked")
    private Class<M> persistentClass = (Class<M>) ((ParameterizedType) this.getClass().getGenericSuperclass())
            .getActualTypeArguments()[0];

    public M create(M obj) {
        this.jpaApi.em().persist(obj);
        return obj;
    }

    public void delete(M obj) {
        this.jpaApi.em().remove(obj);
    }

    public M read(Integer id) {
        if (id != null) {
            M m = this.jpaApi.em().find(this.persistentClass, id);
            if (null != m) {
                return m;
            }
        }
        return null;
    }
    
    public M update(M obj) throws IllegalArgumentException {
        M res = this.jpaApi.em().merge(obj);
        this.jpaApi.em().flush();
        return res;
    }

}

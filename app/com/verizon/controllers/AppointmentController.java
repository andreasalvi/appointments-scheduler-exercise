package com.verizon.controllers;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.verizon.dao.AppointmentDao;
import com.verizon.models.Appointment;

import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Results;

public class AppointmentController extends GenericController<Appointment, AppointmentDao> {

    @Transactional
    public final Result getWithFilters(Long dateMs, Integer userId) {
        if(null != dateMs) {
            if(null != userId) {
                List<Appointment> obj = this.dao.getAppointmentsByDateAndUserId(new Date(dateMs), userId);
                return Results.ok(Json.toJson(obj));
            } else {
                List<Appointment> obj = this.dao.getAppointmentsByDate(new Date(dateMs));
                return Results.ok(Json.toJson(obj));    
            }
        }
        return Results.badRequest();
    }

    @Transactional
    public Result changeDate() {
        JsonNode jsonParam = AppointmentController.request().body().asJson();
        Integer appointmentId = Integer.parseInt(jsonParam.get("appointmentId").asText());
        Long dateMs = Long.parseLong(jsonParam.get("dateMs").asText());

        Appointment appointment = this.dao.read(appointmentId);
        appointment.setDate(new Date(dateMs));
        return Results.ok();
    }

}

package com.verizon.controllers;

import java.lang.reflect.ParameterizedType;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import com.verizon.dao.GenericDao;
import com.verizon.models.GenericModel;

import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;

@SuppressWarnings("deprecation")
public abstract class GenericController<M extends GenericModel, D extends GenericDao<M>> extends Controller {

    @Inject
    protected D dao;

    @SuppressWarnings("unchecked")
    protected Class<M> persistentClass = (Class<M>) ((ParameterizedType) this.getClass().getGenericSuperclass())
            .getActualTypeArguments()[0];

    @Transactional
    public final Result create() {
        JsonNode inputJson = GenericController.request().body().asJson();
        M inputObj = Json.fromJson(inputJson, this.persistentClass);
        if (null == inputObj) {
            return Results.badRequest("No content received");
        }
        M created = this.dao.create(inputObj);        
        return Results.created(Json.toJson(created.getId()));
    }

    @Transactional
    public final Result delete(Integer id) {
        M objToDelete = this.dao.read(id);
        this.dao.delete(objToDelete);
        return Results.ok();
    }

    @Transactional(readOnly = true)
    public final Result get(Integer id) {
        M obj = this.dao.read(id);
        if (null != obj) {
            return Results.ok(Json.toJson(obj));
        } else {
            return Results.notFound();
        }
    }
    
    @Transactional
    public final Result update() {
        JsonNode inputJson = Controller.request().body().asJson();

        M inputObj = Json.fromJson(inputJson, this.persistentClass);
        if (null == inputObj) {
            return Results.badRequest("No content received");
        }
        // this.restoreFromDb(param);
        M updated = null;
        updated = this.dao.update(inputObj);
        this.dao.jpaApi.em().flush();
        return Results.ok(Json.toJson(updated));
    }

}

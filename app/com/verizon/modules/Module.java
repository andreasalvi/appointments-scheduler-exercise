package com.verizon.modules;

import com.google.inject.AbstractModule;
import com.verizon.utility.StartUp;

public class Module extends AbstractModule {

    public void configure() {
        this.bind(StartUp.class).asEagerSingleton();
    }
}

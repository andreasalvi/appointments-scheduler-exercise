package com.verizon.models;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class GenericModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public abstract Integer getId();

    @Override
    public boolean equals(Object obj) {
        try {
            return obj != null && this.getClass().isAssignableFrom(obj.getClass())
                    && this.getId().equals(((GenericModel) obj).getId());
        } catch (Exception e) {
            return false;
        }
    }

}

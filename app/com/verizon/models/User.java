package com.verizon.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User extends GenericModel {

    private Integer id;
    private String fullName;
    private String email;
//    private List<Appointment> appointmentsCreated = new ArrayList<>(0);
//    private List<Meeting> meetingsAttended = new ArrayList<>(0);

    @Id
    @Override
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Column(nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
//    public List<Appointment> getAppointmentsCreated() {
//        return appointmentsCreated;
//    }
//
//    public void setAppointmentsCreated(List<Appointment> appointmentsCreated) {
//        this.appointmentsCreated = appointmentsCreated;
//    }
//
//    @ManyToMany(mappedBy = "participants")
//    public List<Meeting> getMeetingsAttended() {
//        return meetingsAttended;
//    }
//
//    public void setMeetingsAttended(List<Meeting> meetingsAttended) {
//        this.meetingsAttended = meetingsAttended;
//    }

}

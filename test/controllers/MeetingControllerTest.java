package controllers;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.GET;
import static play.test.Helpers.route;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.verizon.models.Appointment;
import com.verizon.models.Meeting;

import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Http.Status;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;

public class MeetingControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void getStatus() {
        Http.RequestBuilder request = new Http.RequestBuilder().method(GET).uri("/api/meeting/1");
        Result result = route(app, request);
        assertEquals(Status.OK, result.status());
        assertEquals("application/json", result.contentType().get());
    }

    @Test
    public void getNotFound() {
        Http.RequestBuilder request = new Http.RequestBuilder().method(GET).uri("/api/meeting/0");
        Result result = route(app, request);
        assertEquals(Status.NOT_FOUND, result.status());
    }

    @Test
    public void getResult() {
        Http.RequestBuilder request = new Http.RequestBuilder().method(GET).uri("/api/meeting/1");
        Result result = route(app, request);
        JsonNode node = Json.parse(Helpers.contentAsString(result));
        Appointment appointment = Json.fromJson(node, Meeting.class);
        assertEquals(appointment.getId().intValue(), 1);
    }

}

Installation
---
Install sbt\
https://www.scala-sbt.org/download.html

Create a DB with name `appointments`. The credentials for the DB can be found in the file `conf/application.conf`.

In the project folder launch
```
sbt compile
sbt run
```

You can use `assets/populate.sql` to fill the DB with some data or use  the dump `assets/appointments-24-02-2020.sql`.

_N.B._: the DB is initialized after first api call.

![appointments calendar](assets/calendar-example.jpg)


Model
---

![Class diagram](assets/class-diagram.jpg)

Routes
---
```
####################### USER ####################### 
PATCH       /api/user                       com.verizon.controllers.UserController.update()
POST        /api/user                       com.verizon.controllers.UserController.create()
GET         /api/user/:id                   com.verizon.controllers.UserController.get(id: Integer)

#################### APPOINTMENT #######################
GET         /api/appointment/:id            com.verizon.controllers.AppointmentController.get(id: Integer)
PATCH       /api/appointment/changeDate     com.verizon.controllers.AppointmentController.changeDate()
GET         /api/appointment                com.verizon.controllers.AppointmentController.getWithFilters(dateMs: Long, userId: Integer ?= null)

#################### REMINDER #######################
PATCH       /api/reminder                   com.verizon.controllers.ReminderController.update()
POST        /api/reminder                   com.verizon.controllers.ReminderController.create()
GET         /api/reminder/:id               com.verizon.controllers.ReminderController.get(id: Integer)

#################### MEETING #######################
PATCH       /api/meeting                    com.verizon.controllers.MeetingController.update()
POST        /api/meeting                    com.verizon.controllers.MeetingController.create()
GET         /api/meeting/:id                com.verizon.controllers.MeetingController.get(id: Integer)
```

To have some call examples you can use Postman importing this collection of requests `assets/appointments.postman_collection.json`.

Test
---
```
sbt test
```


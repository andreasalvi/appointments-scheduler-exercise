name := """appointments"""
organization := "com.verizon"

javaSource in Compile := baseDirectory.value / "app"
javaSource in Test := baseDirectory.value / "test"
javaOptions in Test += "-Dconfig.file=conf/application.test.conf"
resourceDirectory in Test := baseDirectory.value / "test" / "resources"
logLevel := Level.Error

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.6"

libraryDependencies += javaWs
libraryDependencies += javaJpa
libraryDependencies += javaJdbc % Test
libraryDependencies += ws
libraryDependencies += guice

libraryDependencies += "org.hibernate" % "hibernate-core" % "5.1.10.Final"
libraryDependencies += "org.hibernate" % "hibernate-envers" % "5.1.10.Final"
libraryDependencies += "org.hibernate" % "hibernate-ehcache" % "5.1.10.Final"
libraryDependencies += "org.hibernate" % "hibernate-entitymanager" % "5.1.10.Final"
libraryDependencies += "org.hibernate" % "hibernate-search-elasticsearch" % "5.6.4.Final"
libraryDependencies += "org.hibernate" % "hibernate-search-engine" % "5.6.4.Final"
libraryDependencies += "org.hibernate" % "hibernate-search-orm" % "5.6.4.Final"

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.47"

libraryDependencies += "joda-time" % "joda-time" % "2.9.6"

libraryDependencies += "org.reflections" % "reflections" % "0.9.11"
libraryDependencies += "org.apache.commons" % "commons-csv" % "1.6"


EclipseKeys.preTasks := Seq(compile in Compile)
EclipseKeys.projectFlavor := EclipseProjectFlavor.Java           // Java project. Don't expect Scala IDE
EclipseKeys.createSrc := EclipseCreateSrc.ValueSet(EclipseCreateSrc.ManagedClasses, EclipseCreateSrc.ManagedResources)  // Use .class files instead of generated .scala files for views and routes
PlayKeys.externalizeResources := false
Keys.fork in Test := false
parallelExecution in Test := false